import { makeStyles } from '@material-ui/styles';

const useStyles = makeStyles({
  main: {
    marginLeft: '50px',
    width: '790px',
    height: '100%',

    display: 'flex',
    alignItems: 'center',
  },
  mainContainer: {
    width: '100%',
    height: '50%',
  },
  logoContainer: {
    width: '80%',
    display: 'flex',
    justifyContent: 'center',
    flexWrap: 'wrap',
    marginLeft: '20px',

    '& img': {
      width: '400px',
      marginBottom: '40px',
    },
  },
  logoTextContainer: {
    width: '85%',
    textAlign: 'center',

    '& span': {
      fontSize: '18px',
      color: '#aab2c1',
    },
  },
  connectButtonContainer: {
    paddingLeft: '40px',
    width: 'max-content',
    display: 'flex',
    justifyContent: 'flex-start',
    marginTop: '-20px',
  },
  connectButton: {
    display: 'flex',
    justifyContent: 'center',
    flexWrap: 'nowrap',
    marginLeft: '20px',
    marginTop: '120px',

    '& img': {
      width: '280px',
    },
  },
  smallButton: {
    marginLeft: '-25px',
    display: 'flex',
    justifyContent: 'center',
    flexWrap: 'nowrap',
    marginTop: '120px',

    '& img': {
      width: '150px',
    },
  },
  contactButtonContainer: {
    paddingLeft: '60px',
    width: '100%',
    height: '260px',

    display: 'flex',
    alignItems: 'flex-end',

    '& button': {
      backgroundColor: '#fff',
      padding: '14px 55px',
      boxShadow: '0 20px 30px -16px rgba(9,9,16,0.15)!important;',

      fontSize: '16px',
      fontWeight: '500',
      color: '#aab2c1',
    },
  },
  icon: {
    marginLeft: '10px',
    color: '#dadadb',
  },
  formContainer: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  input: {
    marginTop: '20px !important',
  },
  sendButton: {
    marginTop: '30px !important',

    '& img': {
      width: '220px',
    },
  },
  contactButton: {
    textTransform: 'none',
    backgroundColor: '#fff',
  },
});

export default useStyles;
