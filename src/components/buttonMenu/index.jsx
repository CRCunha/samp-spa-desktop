import useStyles from './styles.jsx';
import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFire } from '@fortawesome/free-solid-svg-icons';
import { motion } from 'framer-motion';
//https://cdn.dribbble.com/users/1919332/screenshots/8768113/media/cf6fd068965e228567c3c7c7fab7f085.jpg

const Home = (props) => {
  const classes = useStyles();
  return (
    <motion.div
      whileHover={{
        scale: 1.05,
        transition: { duration: 0.1 },
      }}
      initial={{ opacity: 0, scale: 0.5 }}
      animate={{ opacity: 1, scale: 1 }}
      transition={{
        default: {
          duration: 0.3,
          ease: [0, 0.71, 0.2, 1.01],
        },
        delay: props.delay,
        scale: {
          type: 'spring',
          damping: 10,
          stiffness: 80,
          restDelta: 0.001,
        },
      }}
    >
      <a href={props.link} target="_blank" className={classes.button}>
        <FontAwesomeIcon className={classes.icon} icon={faFire} />
      </a>
    </motion.div>
  );
};

export default Home;
