import { makeStyles } from '@material-ui/styles';

const useStyles = makeStyles({
  header: {
    width: '100%',
    height: '120px',

    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  headerContainer: {
    width: '95%',
    height: '100%',

    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',

    '& img': {
      width: '150px',
    },
  },
  buttonContainer: {
    width: '210px',
    height: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',

    '& img': {
      marginRight: '30%',
      width: '100%',
      cursor: 'pointer',
      zIndex: '100',
    },
  },
});

export default useStyles;
