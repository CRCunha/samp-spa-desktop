import { makeStyles } from '@material-ui/styles';
import backgroundImage from '../../assets/images/background.jpg';

const useStyles = makeStyles({
  main: {
    height: '100vh',
    width: '100vw',
    backgroundImage: `url(${backgroundImage})`,
    backgroundSize: 'cover',
    overflow: 'hidden',
  },

  mainTop: {
    witdh: '100%',
    height: '58%',

    display: 'flex',
    justifyContent: 'center',
    alignItems: 'flex-end',

    '& img': {
      width: '190px',
      cursor: 'pointer',
    },
  },

  mainBottom: {
    width: '100%',
    height: '40%',

    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flexWrap: 'wrap',
  },
  mainBottomContent: {
    height: 'fit-content',
    padding: '10px',

    display: 'flex',
    justifyContent: 'center',

    '& img': {
      width: '260px',
      cursor: 'pointer',
    },
  },
  smallButtons: {
    width: '100%',
    height: '30px',

    marginTop: '-100px',

    display: 'flex',
    justifyContent: 'center',
    flexWrap: 'wrap',
  },
  buttonData: {
    padding: '0 10px',
    height: '100%',
    backgroundColor: '#1d1d1d',

    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',

    fontSize: '10pt',
    color: '#f4f4f4',
    fontFamily: '"Open Sans",sans-serif',
  },
  buttonStatus: {
    marginLeft: '10px',
    padding: '0 10px',
    height: '100%',
    backgroundColor: '#5d7239',

    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',

    fontSize: '10pt',
    color: '#f4f4f4',
  },
  buttonWipe: {
    marginLeft: '10px',
    padding: '0 10px',
    height: '100%',

    backgroundColor: '#d44129',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',

    fontSize: '10pt',
    color: '#f4f4f4',
  },
  tagsContainer: {
    width: '100%',
    height: '30px',
    marginTop: '10px',

    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },

  buttonTag: {
    marginLeft: '5px',
    marginRight: '5px',
    padding: '0 10px',
    height: '100%',
    backgroundColor: '#20608f',

    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',

    fontSize: '10pt',
    color: '#f4f4f4',
  },
});

export default useStyles;

//#20608f
//#f4f4f4
//#d44129
//#5d7239
