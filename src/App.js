import React from 'react';
import Router from './routes';
import './app.css';
import { BrowserRouter } from 'react-router-dom';
import error from './assets/images/404.jpeg';
//http://cristianapp.com.br.s3-website-sa-east-1.amazonaws.com/

const App = () => {
  return (
    <React.StrictMode>
      <BrowserRouter>
        <div className="App">
          <Router>
            <Router />
          </Router>
        </div>
        <div className="AppNone">
          <img src={error} id="errorImage" alt="404" />
        </div>
      </BrowserRouter>
    </React.StrictMode>
  );
};

export default App;
