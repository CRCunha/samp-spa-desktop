import React from 'react';
import { Routes, Route } from 'react-router-dom';
import '../App';

// Pages
import Home from '../pages/home/index';

const Router = () => {
  return (
    <div>
      <Routes>
        <Route path="/" element={<Home />} />
      </Routes>
    </div>
  );
};

export default Router;
