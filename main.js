const { app, BrowserWindow, Tray, Menu, dialog } = require('electron');
const path = require('path');
let mainWindow;

function createWindow() {
  const win = new BrowserWindow({
    quitOnClose: true,
    appIcon: path.join(__dirname, '/sec/assets/images/rust.ico'),
    width: 1280,
    height: 720,
    transparent: true,
    resizable: false,
    autoHideMenuBar: true,
    center: true,
    titleBarStyle: 'hidden',
    title: 'Dorizo Loucher',
    titleBarOverlay: {
      color: '#bebfba',
      symbolColor: '#d44129',
      height: 30,
    },
    customButtonsOnHover: false,
    icon: __dirname + '/src/assets/images/rust.ico',
    webPreferences: {
      preload: path.join(__dirname, 'preloader.js'),
    },
  });
  win.loadURL('http://localhost:3000');

  win.loadURL(url).then(() => console.log('URL loaded.'));

  let tray = null;
  win.on('minimize', function (event) {
    event.preventDefault();
    win.setSkipTaskbar(true);
    tray = createTray();
  });

  win.on('restore', function (event) {
    win.show();
    win.setSkipTaskbar(false);
    tray.destroy();
  });

  return win;
}

function createTray() {
  let appIcon = new Tray(path.join(__dirname, 'cloud_fun.ico'));
  const contextMenu = Menu.buildFromTemplate([
    {
      label: 'Show',
      click: function () {
        mainWindow.show();
      },
    },
    {
      label: 'Exit',
      click: function () {
        app.isQuiting = true;
        app.quit();
      },
    },
  ]);

  appIcon.on('double-click', function (event) {
    mainWindow.show();
  });
  appIcon.setToolTip('Tray Tutorial');
  appIcon.setContextMenu(contextMenu);
  return appIcon;
}

app.whenReady().then(() => {
  createWindow();

  app.on('active', () => {
    if (BrowserWindow.getAllWindows().length === 0) createWindow();
  });
});

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  if (BrowserWindow.getAllWindows().length === 0) {
    createMainWindow();
  }
});
